// var HLSServer = require('hls-server')
// var http = require('http')

// var server = http.createServer()
// var hls = new HLSServer(server, {
//   provider: {
//     exists: function (req, callback) { // check if a file exists (always called before the below methods)
//       callback(null, true)                 // File exists and is ready to start streaming
//       callback(new Error("Server Error!")) // 500 error
//       callback(null, false)                // 404 error
//     },
//     getManifestStream: function (req, callback) { // return the correct .m3u8 file
//       // "req" is the http request
//       // "callback" must be called with error-first arguments
//       callback(null, myNodeStream)
//       // or
//       callback(new Error("Server error!"), null)
//     },
//     getSegmentStream: function (req, callback) { // return the correct .ts file
//       callback(null, myNodeStream)
//     }
//   }
// })
// server.listen(8000)


// Video Playing Using Create Ream Stream

// const express = require('express')
// const fs = require('fs')
// const path = require('path')
// const app = express()

// // app.use(express.static(path.join(__dirname, 'public')))

// app.get('/', function (req, res) {
//   res.sendFile(path.join(__dirname + '/index.htm'))
// })

// app.get('/video', function (req, res) {
//   const path = 'video/1607684697308LearnCSSCalcIn6Minutes_HD.mp4'
//   const stat = fs.statSync(path)
//   const fileSize = stat.size
//   const range = req.headers.range

//   if (range) {
//     const parts = range.replace(/bytes=/, "").split("-")
//     const start = parseInt(parts[0], 10)
//     const end = parts[1]
//       ? parseInt(parts[1], 10)
//       : fileSize - 1

//     if (start >= fileSize) {
//       res.status(416).send('Requested range not satisfiable\n' + start + ' >= ' + fileSize);
//       return
//     }

//     const chunksize = (end - start) + 1
//     const file = fs.createReadStream(path, { start, end })
//     const head = {
//       'Content-Range': `bytes ${start}-${end}/${fileSize}`,
//       'Accept-Ranges': 'bytes',
//       'Content-Length': chunksize,
//       'Content-Type': 'video/mp4',
//     }

//     res.writeHead(206, head)
//     file.pipe(res)
//     // console.log('file stream', file.pipe(res))
//   } else {
//     const head = {
//       'Content-Length': fileSize,
//       'Content-Type': 'video/mp4',
//     }
//     res.writeHead(200, head)
//     fs.createReadStream(path).pipe(res)
//     console.log('stream', fs.createReadStream(path).pipe(res))
//   }
// })

// app.listen(8000, function () {
//   console.log('Listening on port 8000!')
// })

//Video Playing using Node-Media-server

const express = require('express')
const app = express()

const NodeMediaServer = require('node-media-server');

const config = {
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60
  },
  http: {
    port: 8000,
    mediaroot: '/video',
    allow_origin: '*'
  },
  relay: {
    ffmpeg: '../ffmpeg-4.3.2-2021-02-27-full_build/bin/ffmpeg.exe',
    tasks: [
      {
        app: 'cctv',
        mode: 'static',
        edge: 'rtsp://admin:admin888@192.168.0.149:554/ISAPI/streaming/channels/101',
        name: '0_149_101',
        rtsp_transport: 'tcp' //['udp', 'tcp', 'udp_multicast', 'http']
      }, {
        app: 'iptv',
        mode: 'static',
        edge: 'rtmp://live.hkstv.hk.lxdns.com/live/hks',
        name: 'hks'
      }, {
        app: 'mv',
        mode: 'static',
        edge: '/Volumes/ExtData/Movies/Dancing.Queen-SD.mp4',
        name: 'dq'
      }
    ]
  }
};

var nms = new NodeMediaServer(config)


app.listen(9000, (req, res, next) => {
  console.log('Welcome to the port : 9000')
})

nms.run();


nms.on('prePublish', async (id, StreamPath, args) => {
  let stream_key = getStreamKeyFromStreamPath(StreamPath)
  console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);

})

const getStreamKeyFromStreamPath = (path) => {
  let parts = path.split('/')
  return parts[parts.length - 1]
}


//imp

// trans: {
//   ffmpeg: '../ffmpeg-4.3.2-2021-02-27-full_build/bin/ffmpeg.exe',
//   tasks: [
//     {
//       app: 'live',
//       mp4: true,
//       mp4Flags: '[movflags=frag_keyframe+empty_moov]',
//     }
//   ]
// }